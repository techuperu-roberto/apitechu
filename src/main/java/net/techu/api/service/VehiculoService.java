package net.techu.api.service;

import net.techu.api.models.Vehiculo;

import java.util.List;

public interface VehiculoService {

    List<Vehiculo> findAll();
    public Vehiculo findOne(String id);
    public Vehiculo saveVehiculo(Vehiculo veh);
    public void updateVehiculo(Vehiculo veh);
    public void deleteVehiculo(String id);
}
