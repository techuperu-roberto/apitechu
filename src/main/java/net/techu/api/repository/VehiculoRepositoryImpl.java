package net.techu.api.repository;

import net.techu.api.models.Vehiculo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class VehiculoRepositoryImpl implements VehiculoRepository {
    private final MongoOperations mongoOperations;

    @Autowired
    public VehiculoRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Vehiculo> findAll() {
        List<Vehiculo> vehiculos = this.mongoOperations.find(new Query(), Vehiculo.class);
        return vehiculos;
    }

    @Override
    public Vehiculo findOne(String id) {
        Vehiculo encontrado = this.mongoOperations.findOne(new Query(Criteria.where("id").is(id)),Vehiculo.class);
        return encontrado;
    }

    @Override
    public Vehiculo saveVehiculo(Vehiculo veh) {
        this.mongoOperations.save(veh);
        return findOne(veh.getId());
    }

    @Override
    public void updateVehiculo(Vehiculo veh) {
        this.mongoOperations.save(veh);
    }

    @Override
    public void deleteVehiculo(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("id").is(id)), Vehiculo.class);
    }
}
